import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  contactForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group(
      {
        nombre: ["", Validators.required],
        email: ["", [Validators.required, Validators.email] ],
        asunto: ["", Validators.required],
        mensaje: ["", Validators.required]
      }
    );
  }

  get form() : { [key: string]: AbstractControl }{
    return this.contactForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.contactForm.invalid) {
      return;
    }

    alert(
      "Mensaje enviado con éxito"
    )
  }

  onReset() {
    this.submitted = false;
    this.contactForm.reset();
  }

}
