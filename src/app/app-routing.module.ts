import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SobreMiComponent } from './sobre-mi/sobre-mi.component';
import { CurriculumComponent } from './curriculum/curriculum.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { ContactoComponent } from './contacto/contacto.component';

const routes: Routes = [
  {
    path: "",
    component: SobreMiComponent
  },
  {
    path: "curriculum",
    component: CurriculumComponent
  },
  {
    path: "proyectos",
    component: ProyectosComponent
  },
  {
    path: "contacto",
    component: ContactoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
